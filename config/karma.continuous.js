const karmaCommon = require('./karma.common');

module.exports = function (config) {
    karmaCommon.autoWatch = true;
    karmaCommon.singleRun = false;

    config.set(karmaCommon);
};
