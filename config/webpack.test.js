const helpers = require('./helpers');
const ATL = require('awesome-typescript-loader');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

module.exports = {
    devtool: 'inline-source-map',

    resolve: {
        extensions: ['.ts', '.js']
    },

    module: {
        rules: [
            {
                test: /\.ts$/,
                use: ['awesome-typescript-loader', 'angular2-template-loader']
            },
            {
                test: /\.html$/,
                use: 'html-loader'

            },
            {
                test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
                use: 'null'
            },
            {
                test: /\.css$/,
                exclude: helpers.root('src', 'app'),
                use: 'null'
            },
            {
                test: /\.css$/,
                include: helpers.root('src', 'app'),
                use: 'raw-loader'
            }
        ]
    },
    plugins: [
        new ATL.CheckerPlugin(),
        new HtmlWebpackPlugin({
            template: 'src/index.ejs',
            baseUrl: '/'
        }),
        new webpack.DefinePlugin({
            CAS_URL: JSON.stringify('10.230.13.162:8888'),
            REDIRECT_URL: JSON.stringify('/oauth-callback'),
        })]
}
