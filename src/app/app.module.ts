import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";

import {AppComponent} from "./app.component";
import {HeaderComponent} from "../header/header.component";
import {SidebarComponent} from "../sidebar/sidebar.component";
import {CourseDetailLectorerComponent} from "../course-detail-lectorer/course-detail-lectorer.component";
import {CourseDetailStudentComponent} from "../course-detail-student/course-detail-student.component";
import {CreateCourseComponent} from "../create-course/create-course.component";
import {CreateLectorerComponent} from "../create-lectorer/create-lectorer.component";
import {CreateStudentComponent} from "../create-student/create-student.component";
import {ProfileLectorerComponent} from "../profile-lectorer/profile-lectorer.component";

@NgModule({
    imports: [
        BrowserModule
    ],
    declarations: [
        AppComponent,
        HeaderComponent,
        SidebarComponent,
        CourseDetailLectorerComponent,
        CourseDetailStudentComponent,
        CreateCourseComponent,
        CreateLectorerComponent,
        CreateStudentComponent,
        ProfileLectorerComponent
    ],
    providers: [
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
